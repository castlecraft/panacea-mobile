import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DoctorVisitAreaPage } from './doctor-visit-area.page';

const routes: Routes = [
  {
    path: '',
    component: DoctorVisitAreaPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [DoctorVisitAreaPage],
})
export class DoctorVisitAreaPageModule {}
