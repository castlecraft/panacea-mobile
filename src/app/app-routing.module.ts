import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { CallbackComponent } from './callback/callback.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'callback',
    component: CallbackComponent,
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule),
  },
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule),
  },
  { path: 'daily-activity', loadChildren: './daily-activity/daily-activity.module#DailyActivityPageModule' },
  { path: 'doctor-visit', loadChildren: './doctor-visit/doctor-visit.module#DoctorVisitPageModule' },
  { path: 'doctor-visit-area', loadChildren: './doctor-visit-area/doctor-visit-area.module#DoctorVisitAreaPageModule' },
  { path: 'doctor-visit-medicine', loadChildren: './doctor-visit-medicine/doctor-visit-medicine.module#DoctorVisitMedicinePageModule' },
  { path: 'doctor', loadChildren: './doctor/doctor.module#DoctorPageModule' },
  { path: 'add-doctor', loadChildren: './add-doctor/add-doctor.module#AddDoctorPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
