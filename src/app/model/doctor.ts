export class Doctor {
    serverID: string;
    name: string;
    place: string;
    area: string;

    constructor() {
        this.serverID = '';
        this.name = '';
        this.place = '';
        this.area = '';
    }
}