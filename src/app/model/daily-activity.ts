import { Doctor } from './doctor';
import { Medicine } from './medicine';

export class DailyActivity {
    doctor: Doctor;
    status: string;
    visitNumber: string;
    workType: string;
    medicineList: Array<Medicine>;
    scientificInput: Array<ScientificInput>;
    rootCauseAnalysis: Array<string>;
    mitigationPlan: Array<string>;
    remark: string;

    constructor(){
        this.doctor = new Doctor();
        this.status = 'Pending';
        this.visitNumber = '';
        this.workType = '';
        this.medicineList = [];
        this.scientificInput = [];
        this.rootCauseAnalysis = [];
        this.mitigationPlan = [];
        this.remark = '';
    }

}

export class ScientificInput {
    medicine: Array<Medicine>;
    input: string;
}
