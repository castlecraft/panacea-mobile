export class Medicine {
    name: string;
    quantity: number;
    rxPatients: number;
    rx: boolean;
    result: boolean;

    constructor(){
        this.name = '';
        this.quantity = null;
        this.rxPatients = null;
        this.rx = false;
        this.result = false;
    }

}