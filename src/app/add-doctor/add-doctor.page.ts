import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { Doctor } from '../model/doctor';
import { DoctorService } from '../services/doctor.service';

@Component({
  selector: 'app-add-doctor',
  templateUrl: './add-doctor.page.html',
  styleUrls: ['./add-doctor.page.scss'],
})
export class AddDoctorPage implements OnInit {

  doctor: Doctor;
  passedFrom: string;

  constructor(
    private navParams: NavParams,
    private doctorService: DoctorService,
    private modalCtrl: ModalController,
  ) {
    this.passedFrom = navParams.get('passedFrom');
    this.doctor = new Doctor();
    Object.assign(this.doctor, navParams.get('doctor'));
  }

  ngOnInit() {
  }

  addDoctor() {
    this.doctorService.addDoctor(this.doctor).subscribe({
      next: success => {
        this.modalCtrl.dismiss();
      },
      error: error => { },
    });
  }

  updateDoctor() {
    this.doctorService.updateDoctor(this.doctor).subscribe({
      next: success => {
        this.modalCtrl.dismiss();
      },
      error: error => { },
    });
  }

}
