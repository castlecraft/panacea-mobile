import { Injectable } from '@angular/core';
import { DailyActivity } from '../model/daily-activity';
import { Doctor } from '../model/doctor';
import { HttpClient } from '@angular/common/http';
import { TokenService } from '../api/token.service';
import { APP_URL, ACCESS_TOKEN } from '../api/constants/storage';
import { OAuthProviderClientCredentials } from '../api/constants/oauth2-config';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DailyCallReportService {
  public dailyActivityList: Array<DailyActivity>;
  public doctorList: Array<Doctor>;
  public areaList: Array<string>;

  constructor(
    private http: HttpClient,
    private token: TokenService,
  ) {
    this.dailyActivityList = [];
    this.areaList = [
      // "Thergaon",
      // "Andheri East",
      // "Airoli",
      // "Mysore"
    ];
    this.doctorList = [
      // {
      //   "name": "Lewis Bryan",
      //   "area": "Thergaon",
      //   "place": "Chinchwad"
      // },
      // {
      //   "name": "Jacob Barnett",
      //   "area": "Andheri East",
      //   "place": "Tilak Nagar"
      // },
      // {
      //   "name": "Kareem Watson",
      //   "area": "Thergaon",
      //   "place": "Aundh"
      // },
      // {
      //   "name": "Patrick Joyce",
      //   "area": "Andheri East",
      //   "place": "Saki naka"
      // },
      // {
      //   "name": "Ryder Goodman",
      //   "area": "Airoli",
      //   "place": "Illes Balears"
      // },
      // {
      //   "name": "Hamish Wynn",
      //   "area": "Mysore",
      //   "place": "local"
      // },
      // {
      //   "name": "Simon Glass",
      //   "area": "Airoli",
      //   "place": "Stockholms län"
      // },
    ];
  }

  getDoctorList() {
    const url = OAuthProviderClientCredentials.authServerUrl + '/api/resource/Doctor?fields=["*"]';

    return this.token.getToken().pipe(
      switchMap(token => {
        return this.http.get<{data: any|any[]}>(url, {headers: {
          Authorization: 'Bearer ' + token,
        }});
      }),
    );
  }
}
