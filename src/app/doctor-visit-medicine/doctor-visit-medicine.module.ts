import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DoctorVisitMedicinePage } from './doctor-visit-medicine.page';

const routes: Routes = [
  {
    path: '',
    component: DoctorVisitMedicinePage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [DoctorVisitMedicinePage],
})
export class DoctorVisitMedicinePageModule {}
