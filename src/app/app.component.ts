import { Component, OnInit } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TokenService } from './api/token.service';
import { OAuthProviderClientCredentials } from './api/constants/oauth2-config';
import { Router } from '@angular/router';
import { APP_KEY, SPLASHSCREEN_KEY } from './constants/strings';
import { StorageService } from './api/storage.service';
import { LOGGED_IN } from './api/constants/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  loggedIn: boolean;
  public appPages = [
    {
      title: 'Dashboard',
      url: '/home',
      icon: 'home',
    },
    {
      title: 'Doctor Visit',
      url: '/doctor-visit',
      icon: 'people',
    },
    {
      title: 'Daily Activity',
      url: '/daily-activity',
      icon: 'today',
    },
    {
      title: 'Doctor',
      url: '/doctor',
      icon: 'medkit',
    },
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private token: TokenService,
    private storage: StorageService,
    private router: Router,
    private navController: NavController,
  ) {
    this.initializeApp();
    this.backButtonEvent();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.token.setupOauthConfig(OAuthProviderClientCredentials);
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    this.storage.changes.subscribe(event => {
      if (event.key === LOGGED_IN) {
        this.loggedIn = event.value ? true : false;

        if (this.loggedIn) {
          const initialHref = window.location.href;
          navigator[SPLASHSCREEN_KEY].show();
          window.location.href = initialHref;
        }
      }

    });
    this.loggedIn = localStorage.getItem(LOGGED_IN) ? true : false;
  }

  backButtonEvent() {
    this.platform.backButton.subscribeWithPriority(1, () => {
      const url = this.router.url;
      if (url.match('(^/[a-zA-Z0-9-.]*)$')) {
        navigator[APP_KEY].exitApp();
      } else {
        this.navController.navigateBack(
          url.replace(new RegExp('(/([a-zA-Z0-9-.])*)$'), ''),
        );
      }
    });
  }

  showItem(title: string) {
    return (title === 'Dashboard' || this.loggedIn === true);
  }
}
