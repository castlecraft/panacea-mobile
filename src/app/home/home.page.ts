import { Component } from '@angular/core';
import { TokenService } from '../api/token.service';
import { StorageService } from '../api/storage.service';
import {
  LOGGED_IN,
  ACCESS_TOKEN,
  REFRESH_TOKEN,
  EXPIRES_IN,
} from '../api/constants/storage';
import { SPLASHSCREEN_KEY } from '../constants/strings';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  loggedIn: boolean = false;
  hideAuthButtons: boolean = false;

  constructor(private token: TokenService, private storage: StorageService) {}

  ionViewWillEnter() {
    this.loggedIn = localStorage.getItem(LOGGED_IN) ? true : false;
  }

  login() {
    this.hideAuthButtons = true;
    this.token.initializeCodeGrant();
  }

  logout() {
    this.loggedIn = false;
    this.token.revokeToken();
    this.storage.clear(LOGGED_IN);
    this.storage.clear(ACCESS_TOKEN);
    this.storage.clear(REFRESH_TOKEN);
    this.storage.clear(EXPIRES_IN);
    const initialHref = window.location.href;
    navigator[SPLASHSCREEN_KEY].show();
    window.location.href = initialHref;
  }

  moreOptions() {}
}
