import { Component, OnInit } from '@angular/core';
import { DailyCallReportService } from '../services/daily-call-report.service';
import { Router, NavigationExtras } from '@angular/router';
import { Doctor } from '../model/doctor';

@Component({
  selector: 'app-doctor-visit',
  templateUrl: './doctor-visit.page.html',
  styleUrls: ['./doctor-visit.page.scss'],
})
export class DoctorVisitPage implements OnInit {

  tempAreaList: Array<string>;

  constructor(
    private DCRService: DailyCallReportService,
    private router: Router,
  ) {
    this.tempAreaList = DCRService.areaList;
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.DCRService.getDoctorList()
      .subscribe({
        next: (res) => {
          this.DCRService.doctorList = [];
          res.data.forEach((doctor) => {
            const tempDoctor = new Doctor();
            tempDoctor.name = doctor.doctor_name;
            tempDoctor.area = doctor.area;
            tempDoctor.place = doctor.sub_territory;
            tempDoctor.serverID = doctor.name;

            this.DCRService.doctorList.push(tempDoctor);

            const found = this.DCRService.areaList.some(area => area === doctor.area);
            if (!found) {
              this.DCRService.areaList.push(doctor.area);
            }
          });

        },
      });
  }

  navigate(area: string) {
    const navigationExtras: NavigationExtras = {
      state: {
        area,
      },
    };
    this.router.navigate(['doctor-visit-area'], navigationExtras);
  }

}
